
import { authHeader } from '../_helpers';
import { CONST } from '../_config';

export const userService = {
    login,
    logout,
    getAll
};

function login(username, password) {
    let header = new Headers({
        'Content-Type': 'application/json'
    });
    const requestOptions = {
        method: 'POST',
        headers: header,
        body: JSON.stringify({ username: username, password: password })
    };

    return fetch(CONST.BACKEND_URL + `/brfantasy/public/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            //console.log(JSON.stringify(user.data));

            if (user.data.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user.data));
            }

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(CONST.BACKEND_URL + `/brfantasy/public/api/listsubadmin`, requestOptions)
        .then(handleResponse)
        .then(data => {

            let userObj = {
                listOfUser: data.responsePayload.results
            }
            return userObj;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                //location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}