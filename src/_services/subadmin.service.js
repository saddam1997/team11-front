
import { authHeader } from '../_helpers';
import { CONST } from '../_config';

export const subadminService = {
    getAllSubAdmin
};
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function getAllSubAdmin() {


    const requestOptions = {
        method: "POST",
        headers: new Headers({
            "Content-Type": "multipart/form-data",
            "Authorization": authHeader().Authorization

        })
    }
    return fetch(CONST.BACKEND_URL + `/brfantasy/public/api/listsubadmin`, requestOptions)
        .then(handleResponse)
        .then(res => {
            // login successful if there's a jwt token in the response
            //console.log(JSON.stringify(res.data));
            let userObj = {
                listOfSubAdmins: res.data
            }
            return userObj;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                //location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}