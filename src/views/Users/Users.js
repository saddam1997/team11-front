import React, { Component } from 'react';

import { Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import { connect } from 'react-redux';
import { userActions } from '../../_actions';



function UserRow(props) {
  const user = props.user
  const getBadge = (status) => {
    return status === 1 ? 'success' :
      'danger'
  }

  return (
    <tr key={user.id}>
      <td>{user.id}</td>
      <td>{user.userid}</td>
      <td><Badge color={getBadge(user.status)}>{user.status === 1 ? 'Active' : 'Inactive'}</Badge></td>
    </tr>
  )
}

class Users extends Component {
  componentDidMount() {
    this.props.dispatch(userActions.getAll());
  }
  render() {

    const { users } = this.props;
    let { items } = users;
    console.log("items  " + JSON.stringify(items));
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Users <small className="text-muted">example</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">userId</th>
                      <th scope="col">status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      items ? items.map((user, index) =>
                        <UserRow key={index} user={user} />
                      ) : <UserRow key={1} user={''} />
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}
function mapStateToProps(state) {

  const { users, authentication } = state;

  const { user } = authentication;
  //console.log("users.users  " + JSON.stringify(users));

  return {
    user,
    users
  };
}
//const connectedUsers = connect(mapStateToProps)(Users);

export default connect(mapStateToProps)(Users);
//export { connectedUsers as Users };
//export default Users;
