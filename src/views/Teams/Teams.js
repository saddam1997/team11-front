import React, { Component } from 'react';

import { Card, CardBody, CardHeader, Col, Row, Table, Badge } from 'reactstrap';
import { connect } from 'react-redux';
import { subadminActions } from '../../_actions';



function UserRow(props) {
  const subadm = props.subadmin
  const getBadge = (status) => {
    return status === "1" ? 'success' :
      'danger'
  }

  return (
    <tr key={subadm.id}>
      <td>{subadm.id}</td>
      <td>{subadm.username}</td>
      <td><Badge color={getBadge(subadm.status)}>{subadm.status === "1" ? 'Active' : 'Inactive'}</Badge></td>
    </tr>
  )
}

class Teams extends Component {
  componentDidMount() {
    this.props.dispatch(subadminActions.getAllSubAdmin());
  }

  render() {

    const { subadmin } = this.props;
    let { items } = subadmin;
    //console.log("items >>>>>> " + JSON.stringify(this.props));
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={6}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Users <small className="text-muted">example</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">id</th>
                      <th scope="col">userId</th>
                      <th scope="col">status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      items ? items.map((subadmin, index) =>
                        <UserRow key={index} subadmin={subadmin} />
                      ) : <UserRow key={0} subadmin={''} />
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}
function mapStateToProps(state) {
  const { subadmin, authentication } = state;
  //console.log("state  ", state)
  const { user } = authentication;
  return {
    user,
    subadmin
  };
}
export default connect(mapStateToProps)(Teams);
