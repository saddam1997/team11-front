import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { subadmin } from './subadmin.reducer';

const rootReducer = combineReducers({
  authentication,
  users,
  subadmin,
  alert
});

export default rootReducer;