import { subadminConstants } from '../_constants';

export function subadmin(state = {}, action) {
  switch (action.type) {
    case subadminConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case subadminConstants.GETALL_SUCCESS:
      return {
        items: action.subadmins.listOfSubAdmins
      };
    case subadminConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}