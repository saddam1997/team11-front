import { subadminConstants } from '../_constants';
import { subadminService } from '../_services';

export const subadminActions = {
    getAllSubAdmin
};

function getAllSubAdmin() {
    return dispatch => {
        dispatch(request());

        subadminService.getAllSubAdmin()
            .then(
                subadmins => dispatch(success(subadmins)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: subadminConstants.GETALL_REQUEST } }
    function success(subadmins) { return { type: subadminConstants.GETALL_SUCCESS, subadmins } }
    function failure(error) { return { type: subadminConstants.GETALL_FAILURE, error } }
}